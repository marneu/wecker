 
Tageslichtwecker mit RPi:
  - Ein Tageslichtwecker macht mit hellem Licht und optional Musik die Dämmerung zum helllichten Tag!
  - Weboberfläche zur Einstellung von Uhrzeit, optional Musikstück(e)
  - Funktechnik am RPi siehe "Raspberry Pi - Das umfassende Handbuch", Seite 923ff
  - Hardware:
    - RPi
    - Funksteckdose 434 Mhz
    - RPi-Modul: RF Link Transmitter 434 Mhz, 4 Pin, 5 v (5-7 Euro)
    - www.amazon.de/SparkFun-434MHz-RF-Link-Transmitter/dp/B007XEXICS/ref=sr_1_1
    - 10W-LED
    - Lautsprecher

--------------------------------------------------------------------------------

Funktionsweise:
  => siehe use-case.uxf (UMLet-Datei)
  - Nutzer setzt die Weckzeit in time.txt mittels Texteditor oder Weboberfläche
  (Django-App unter 0.0.0.0:3000/weblog/wecker)
  - time.txt wird von alarm_daemon.py beobachtet
  - wenn Weckzeit erreicht ist, ruft alarm_daemon.py die Programme send und mpc auf
  - send sendet Signal an Funksteckdose, an die eine Lampe angeschlossen ist
  - mpc weist mpd an, eine Audiodatei abzuspielen
  - Nutzer deaktiviert den Alarm, indem er eine neue Uhrzeit in time.txt schreibt
  (Texteditor oder Weboberfläche)

Abhängigkeiten:
  wiringPi
  rcswitch-pi
  mpc, mpd

  Abhängigkeiten werden durch wiringpi.sh und rcswitch.sh installiert. Siehe
  https://www.raspberrypi.org/forums/viewtopic.php?f=37&t=66946 oder
  "Raspberry Pi - Das umfassende Handbuch", Seite 932

  Installation kann durch die Befehle "gpio -v" und "send" getestet werden.

GPIO-Pin:
  11 (BCM), 17(BOARD)
  Der Pin muss bei wiringPi eingestellt werden, offenbar VOR der Kompilierung, und
  zwar in send.cpp. Unter folgender Internetseite wird erläutert, welcher Pin
  standardmäßig verwendet wird:

    http://wiringpi.com/pins/

  Leider nicht sehr verständlich, da sich die Bennenung der Pins zwischen den
  vielen Versionen des RPi stark unterscheidet. Beim RPi 3 entspricht "Pin = 0" in
  send.cpp GPIO-Pin 11(Board) bzw. 17(BCM).
  Siehe Befehl "gpio readall".

Schaltung:
  siehe "Raspberry Pi - Das umfassende Handbuch", Seite 931, Abb. 32.2

Systemcode:
  10000(voreingestellt)

Gerätecode:
  1(voreingestellt)

Befehl:
  sudo send <Systemcode> <Gerätecode> <Status: Boolean>
  an:   sudo send 10000 1 1
  aus:  sudo send 10000 1 0
