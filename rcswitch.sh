#!/bin/bash
# installations-skript
# siehe: https://www.raspberrypi.org/forums/viewtopic.php?f=37&t=66946
# siehe: "Raspberry Pi - Das umfassende Handbuch" seite 932
# wiringPi muss zuerst installiert sein

git clone https://github.com/r10r/rcswitch-pi.git
cd rcswitch-pi
make
cp send /usr/bin

exit
