#!/usr/bin/python3
"""
Hintergrundprozess, der einen Alarm auslöst, wenn die Zeit gekommen ist.
Zeit einstellen: http://raspi:3000/weblog/wecker
"""
import os
from time import sleep
from datetime import datetime
from pytz import timezone

file_path = "time.txt"
localtz = timezone('Europe/Berlin')
sound_file = "Into_The_Labyrinth_60fps_-_Anime_MV_♫_AMV.mp3"

def now_datetime():
    """ timezone-aware time """
    return localtz.localize(datetime.now())

def time_til_alarm():
    """ returns time left until alarm is to be rang """
    alarm_t = open(file_path, "r").read().rstrip()
    alarm_datetime = datetime.strptime(alarm_t, "%Y-%m-%d %H:%M:%S %z")
    diff_t = alarm_datetime - now_datetime()
    # print(diff_t.total_seconds())
    return diff_t

def switch_light(mode):
    """ control light via radio """
    if   mode == "on":
        os.system("sudo send 10000 1 1")
    elif mode == "off":
        os.system("sudo send 10000 1 0")

def play_sound(mode):
    """ start sound playback with mpd via mpc """
    if   mode == "on":
        os.system("mpc clear")
        os.system("mpc add %s" % sound_file)
        os.system("mpc play")
    elif mode == "off":
        os.system("mpc pause")

def alarm(mode):
    if   mode == "on":
        switch_light("on")
        play_sound("on")
    elif mode == "off":
        switch_light("off")
        play_sound("off")

# initialize and reset variables
sent = False
alarm("off")
print("daemon started %s" % str(now_datetime()))

while True:
    diff_t = time_til_alarm()
    if   diff_t.total_seconds() > 0 and sent==True:
        alarm("off")
        print("alarm off %s" % str(now_datetime()))
        sent = False
    elif diff_t.total_seconds() <= 0 and sent==False:
        alarm("on")
        print("alarm on %s" % str(now_datetime()))
        sent = True
    sleep(3)
    # print("%s ist die Zeit" % str(now_datetime()))
